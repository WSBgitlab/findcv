# image name and version
FROM python:3.8

# WORKDIR
WORKDIR /usr/src

#COPY FILES
COPY requirements.txt ./

COPY requirements/ ./requirements

# TIMEZONE SAO PAULO/BRASIL
ENV TZ=America/Sao_Paulo

RUN pip3 install --no-cache-dir -r ./requirements.txt

RUN rm requirements.txt

RUN mkdir /findcv

COPY ./src/findcv ./findcv



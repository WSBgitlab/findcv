settings=findcv.settings

VERSION := 1.6.3

ifdef SIMPLE_SETTINGS
	settings=$(SIMPLE_SETTINGS)
else
	export SIMPLE_SETTINGS=$(settings)
endif

export PYTHONPATH=$(shell pwd)/src/
export PYTHONDONTWRITEBYTECODE=1
export DJANGO_SETTINGS_MODULE=$(settings)

.PHONY: help

run-dev:
	@gunicorn findcv:app --bind localhost:8080 --worker-class aiohttp.worker.GunicornUVLoopWebWorker -e SIMPLE_SETTINGS=findcv.settings -e APM_ENV='development' --reload
from aiohttp import web
from aiohttp_cors import CorsViewMixin
from http import HTTPStatus

from findcv.version import __version__

class PingView(web.View, CorsViewMixin):
    async def get(self):
        return web.json_response({'ok': True, 'version': __version__})

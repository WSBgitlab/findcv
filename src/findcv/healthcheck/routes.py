from .views import PingView

HEALTHCHECK_ROUTES = [
    ('GET', '/healthcheck/', PingView.get, 'ping')
]
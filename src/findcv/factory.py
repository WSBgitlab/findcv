import aiohttp_cors
from aiohttp import web
from aiocache import caches
from simple_settings import settings

from .routes import setup_routes

def build_app():
    caches.set_config(settings.AIO_CACHES)
    app = web.Application()

    cors_allowed_hosts = {
        host: aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
            allow_methods="*"
        )
        for host in settings.CORS_ALLOWED_HOSTS.split()
    }

    cors = aiohttp_cors.setup(app, defaults=cors_allowed_hosts)

    setup_routes(app, cors)

    return app
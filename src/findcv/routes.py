from findcv.healthcheck.routes import HEALTHCHECK_ROUTES

ROUTES = []

ROUTES += HEALTHCHECK_ROUTES

def setup_routes(app, cors):
    for http_method, endpoint, handler, name in ROUTES:
        cors.add(
            app.router.add_route(
                method=http_method, path=endpoint, handler=handler, name=name
            )
        )
